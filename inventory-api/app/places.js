const express = require('express');


const createRouter = connection => {


    const router = express.Router();


    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `places`', (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'})
            }
            res.send(results);
        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `places` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'})
            }

            if (results[0]) {
                res.send(results[0]);
            } else {
                res.status(404).send({error: 'Item not found'});
            }

        });
    });

    router.post('/', (req, res) => {
        const places = req.body;


        connection.query('INSERT INTO `places` (`title`, `description`) VALUES (?, ?)',
            [places.title, places.description],
            (error, results) => {
                if (error) {
                    res.status(500).send({error: 'Database error'})
                }
                res.send({message: 'OK'})
            }
        );
    });


    return router
};


module.exports = createRouter;