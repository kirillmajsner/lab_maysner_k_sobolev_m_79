CREATE SCHEMA `inventory` DEFAULT CHARACTER SET utf8 ;

USE `inventory` ;

CREATE TABLE `categories` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT
) ;

CREATE TABLE `places` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT
) ;

CREATE TABLE `inventory`.`items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category_id` INT NULL,
  `places_id` INT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT(1000) NULL,
  `image` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `category_id_fk_idx` (`category_id` ASC),
  INDEX `places_id_fk_idx` (`places_id` ASC),
  CONSTRAINT `category_id_fk`
    FOREIGN KEY (`category_id`)
    REFERENCES `inventory`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `places_id_fk`
    FOREIGN KEY (`places_id`)
    REFERENCES `inventory`.`places` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE);

    INSERT INTO `categories` (`id`, `title`, `description`)
    VALUES
    (10, 'furniture', 'Some descr'),
    (11, 'computer equipment', 'Some descr'),
    (12, 'appliances', 'Some descr');

    INSERT INTO `places` (`id`, `title`, `description`)
    VALUES
    (21, 'teachers room', 'Some descr'),
    (22, 'directors office' ,'Some descr'),
    (23, 'class #4' ,'Some descr');

    INSERT INTO `items` (`id`, `category_id`, `places_id`, `title`, `description`)
    VALUES
    (31, 10, 21, 'chear', 'some descr'),
    (32, 11, 22, 'monitor', 'some descr'),
    (33, 12, 23, 'microwave', 'some descr');
